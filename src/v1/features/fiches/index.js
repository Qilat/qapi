const Fiches = require('./fiches');
const {Validator} = require("../../middlewares");

module.exports = (router) => {
    router.route('/')
        .get(
            Validator({
                name: {
                    type: 'string',
                    optional: true
                }
            }),
            Fiches.readAll);
    return router;
};
