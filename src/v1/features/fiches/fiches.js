const {Responder, StandardErrors} = require('../../utils');
const fs = require("fs");
const config = require("../../../config");

checkifIsChildOfFolder = (parent, file) => {
    const path = require('path');
    const relative = path.relative(parent, file);
    return relative && !relative.startsWith('..') && !path.isAbsolute(relative);
}

module.exports.readAll = (req, res, next) => {
    const connection = {req, res, next};

    const name = req.query.name;

    if (!name) {
        let files = [];
        fs.readdirSync(config.fiches.folder)
            .forEach(
                file => {
                    const {size, mtime} = fs.statSync(config.fiches.folder + '/' + file)


                    files.push({
                        name: file,
                        size,
                        mtime
                    })
                }
            );
        Responder.sendSuccessWithData(connection, files)
    } else {
        let absolutePath = config.fiches.folder + '/' + req.query.name;
        if (checkifIsChildOfFolder(config.fiches.folder, absolutePath)) {
            const file = fs.lstatSync(absolutePath)
            if (fs.existsSync(absolutePath) && file.isFile()) {
                res.sendFile(absolutePath);
            } else {
                Responder.sendError(connection, StandardErrors.OBJECT_NOT_FOUND);
            }
        } else {
            console.log('forbidden')
            Responder.sendError(connection, StandardErrors.FORBIDDEN);
        }
    }
};
