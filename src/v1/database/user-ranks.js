const {Error, StandardErrors, Table} = require('../utils');

const userRanks = new Table('user_ranks',
    {
        'userId': 'user_id',
        'rankId': 'rank_id'
    },
    {
        'user_id': 'userId',
        'rank_id': 'rankId'
    }
);

userRanks.selectByUserId = (sql, userId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT rank_id FROM `user_ranks` WHERE user_id = ?', [userId],
                (err, result) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            resolve(result.map(line => line['rank_id']));
                        } else {
                            resolve([]);
                        }
                    }
                }
            );
        }
    )
}

userRanks.delete = (sql, userId, rankId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('DELETE FROM `user_ranks` WHERE user_id = ? AND rank_id = ?', [userId, rankId],
                (err) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    )
}


module.exports = userRanks;
