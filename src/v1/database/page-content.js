const {StandardErrors, Table} = require('../utils');

const pageContent = new Table('page_content',
    {
        'id': 'id',
        'name': 'name',
        'htmlContent': 'html_content',
        'lastModifiedBy': 'last_modified_by'
    },
    {
        'id': 'id',
        'name': 'name',
        'html_content': 'htmlContent',
        'last_modified_by': 'lastModifiedBy'
    });

pageContent.insert = (sql, values) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('INSERT INTO page_content (`name`, `htmlContent`, `lastModifiedBy`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE htmlContent = ?',
                [values.name, values.htmlContent, values.lastModifiedBy, values.htmlContent],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                });
        });
};
pageContent.selectByName = (sql, name) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT * FROM page_content WHERE `name` = ?', [name],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            resolve(result[0])
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                }
            );
        }
    );
};

pageContent.selectNames = (sql) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT `name` FROM page_content', [],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    )
};

module.exports = pageContent;
