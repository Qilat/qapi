const {StandardErrors, Table, SQL} = require('../utils');

const terrains = new Table('terrains');
terrains.updateByYearAndPromotion = (sql, yearId, promotionId, data) => {
    return new Promise(
        (resolve, reject) => {

            let query = '';
            const args = [];

            const toUpdate = data['toUpdate'];

            for (const line of toUpdate) {
                if (line.id) {
                    query += 'UPDATE terrains SET maxAmountPlaces = ? WHERE id = ?;';
                    args.push(line.maxAmountPlaces, line.id);
                } else {
                    query += 'INSERT INTO terrains (yearId, promotionId, periodId, terrainTypeId, maxAmountPlaces) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE maxAmountPlaces = ?;';
                    args.push(yearId, promotionId, line.periodId, line.terrainTypeId, line.maxAmountPlaces, line.maxAmountPlaces);
                }
            }

            const toDelete = data['toDelete'];

            for (const id of toDelete) {
                query += 'DELETE FROM terrains WHERE id = ?;';
                args.push(id);
            }

            sql.query(query, args,
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    );
};
terrains.selectByYearAndPromotion = (sql, fields, yearId, promotionsId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ' + SQL.getFormattedFields(fields) + ' FROM `terrains` WHERE yearId = ? AND promotionId = ? ', [yearId, promotionsId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                })
        }
    )
};
terrains.selectByTerrainTypeAndPeriod = (sql, fields, terrainTypeIds, periodsId) => {
    return new Promise(
        (resolve, reject) => {

            if (!Array.isArray(terrainTypeIds))
                terrainTypeIds = [terrainTypeIds];

            sql.query('SELECT ?? FROM `terrains` WHERE terrainTypeId IN (?)  AND periodId IN (?)', [fields, terrainTypeIds, periodsId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                })
        }
    )

};
terrains.selectConstraintsById = (sql, terrainId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query("SELECT t.id AS terrainId, tt.id AS terrainTypeId, t.periodId AS periodId, t.maxAmountPlaces AS maxAmountPlaces, tt.maxAttributions AS maxAttributionPerTerrainType, tt.maxAttributionsPerYear AS maxAttributionsPerYearPerTerrainType FROM terrains t INNER JOIN terrain_types tt ON t.terrainTypeId = tt.id WHERE t.id = ?;",
                [terrainId],
                (error, terrainConstraints) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (terrainConstraints.length > 0) {
                            const constraints = {};
                            constraints.terrainConstraints = terrainConstraints[0];

                            sql.query(
                                "SELECT ca.id                     AS categoryId, " +
                                "       ca.maxAttributions        AS maxAttributionsPerCategory, " +
                                "       ca.maxAttributionsPerYear AS maxAttributionsPerYearPerCategory, " +
                                "       ca.maxChoices             AS maxChoicesPerCategory, " +
                                "       (SELECT CONCAT(\'[\', GROUP_CONCAT(c.terrainTypeId), \']\') FROM category_attributions c WHERE c.categoryId = ca.id) AS terrainTypeIds " +
                                "FROM categories ca " +
                                "         INNER JOIN category_attributions c ON ca.id = c.categoryId " +
                                "         INNER JOIN terrain_types tt ON c.terrainTypeId = tt.id " +
                                "         INNER JOIN terrains t ON tt.id = t.terrainTypeId " +
                                "WHERE t.id = ?",
                                [terrainId],
                                (error, categoriesConstraints) => {
                                    if (error) {
                                        console.error(error);
                                        reject(StandardErrors.SQL_ERROR);
                                    } else {
                                        constraints.categoriesConstraints = categoriesConstraints;
                                        for (let i = 0; i < constraints.categoriesConstraints.length; i++) {
                                            constraints.categoriesConstraints[i].terrainTypeIds = JSON.parse(constraints.categoriesConstraints[i].terrainTypeIds);
                                        }
                                        resolve(constraints);
                                    }
                                });
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                });
        }
    );
};

module.exports = terrains;
