const {StandardErrors, Table} = require('../utils');

const documents = new Table('documents',
    {
        'id': 'id',
        'name': 'name',
        'description': 'description',
        'promotionId': 'promotion_id',
        'fileId': 'file_id'
    },
    {
        'id': 'id',
        'name': 'name',
        'description': 'description',
        'promotion_id': 'promotionId',
        'file_id': 'fileId'
    });

documents.selectAll = (sql) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT d.id, d.name, d.description, d.promotionId, f.name AS fileName FROM documents d INNER JOIN files f ON f.id = d.fileId',
                [],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                });
        }
    )
};

documents.getListByPromotion = (sql, promotionId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT d.id, d.name, d.description, f.name AS fileName FROM documents d INNER JOIN files f ON f.id = d.fileId WHERE d.promotionId = ? OR d.promotionId = 0',
                [promotionId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            )
        }
    );
};

documents.getAttachedFile = (sql, documentId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT f.uuid, f.name FROM documents d INNER JOIN files f ON f.id = d.fileId WHERE d.id = ?',
                [documentId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            resolve(result[0]);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                })
        }
    );
};

module.exports = documents;
