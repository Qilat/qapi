const {StandardErrors, Table} = require('../utils');

const logs = new Table('logs',
    {
        'id': 'id',
        'date': 'date',
        'executorUserId': 'executor_user_id',
        'targetUserId': 'target_user_id',
        'executorUserIp': 'executor_user_ip',
        'type': 'type',
        'data': 'data'
    },
    {
        'id': 'id',
        'date': 'date',
        'executor_user_id': 'executorUserId',
        'target_user_id': 'targetUserId',
        'executor_user_ip': 'executorUserIp',
        'type': 'type',
        'data': 'data'
    });

logs.insert = (sql, values) => {

    values = Array.isArray(values) ? values : [values];

    let query = '';
    const params = [];
    for (const value of values) {
        query += "INSERT INTO `logs` (`date`, `executor_user_id`, `target_user_id`, `executor_user_ip`, `type`, `data`) VALUES (?, ?, ?, ?, ?, ?); ";
        params.push(value.date, value.executor_user_id, value.target_user_id, value.executor_user_ip, value.type, value.data);
    }

    return new Promise(
        (resolve, reject) => {
            sql.query(query, params,
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    )

};
logs.selectSpecificLog = (sql, executorId, targetId, actionType) => {
    return new Promise(
        (resolve, reject) => {
            let query = 'SELECT id, `date`, executor_user_id as executorId, target_user_id as targetId, executor_user_ip as executorIp, type, `data` FROM `logs` WHERE `type` = ? ';
            let params = [actionType];
            if (executorId && executorId > 0) {
                query += ' AND executor_user_id = ? ';
                params.push(executorId);
            }
            if (targetId && targetId > 0) {
                query += ' AND target_user_id = ?';
                params.push(targetId);
            }

            sql.query(query, params,
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        result.forEach(
                            (e) => {
                                e.data = JSON.parse(e.data);
                            }
                        );
                        resolve(result);
                    }
                }
            );

        }
    );
};

module.exports = logs;
