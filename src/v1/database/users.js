const {StandardErrors, Table} = require('../utils');

const users = new Table('users',
    {
        'id': 'id',
        'firstName': 'first_name',
        'lastName': 'last_name',
        'email': 'email',
        'phone': 'phone',
        'hash': 'hash'
    },
    {
        'id': 'id',
        'first_name': 'firstName',
        'last_name': 'lastName',
        'email': 'email',
        'phone': 'phone',
        'hash': 'hash'
    });

users.selectByEmail = (sql, email, fields) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM `' + users.tableName + '` WHERE `email` = ?', [fields, email],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            const returnedResult = users.transformObjectSqlToJs(result[0]);
                            resolve(returnedResult);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                }
            );
        }
    );
};

users.selectByPromotion = (sql, promotionId, fields) => {
    return new Promise(
        (resolve, reject) => {
            sql.query("SELECT ?? FROM `' + users.tableName + '` WHERE promotionId = ?", [fields, promotionId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};

users.selectAllInUserPromotion = (sql, userId, fields) => {
    return new Promise(
        (resolve, reject) => {
            sql.query("SELECT ?? FROM `' + users.tableName + '` WHERE promotionId = (SELECT promotionId FROM users WHERE id = ?)", [fields, userId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};

users.updateGroupByTerrainId = (sql, groupId, groupBlocked, terrainId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('UPDATE `' + users.tableName + '` SET `group` = ?, `groupBlocked` = ? WHERE `' + users.tableName + '`.id IN (SELECT terrain_attributions.userId FROM terrain_attributions WHERE terrain_attributions.terrainId = ?)',
                [groupId, groupBlocked, terrainId],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                })
        }
    );
};

users.countUsers = (sql) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT COUNT(*) AS amount FROM `' + users.tableName + '`',
                [],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result[0].amount);
                    }
                }
            )
        }
    );
};

users.updateMultipleUsersRedoublingState = (sql, ids, redoubling) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('UPDATE `' + users.tableName + '` SET redoubling = ? WHERE id IN (?)',
                [redoubling, ids],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            )
        }
    );
};

users.switchNotRedoublingToPromotion = (sql, promotionFromId, promotionToId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('UPDATE `' + users.tableName + '` SET promotionId = ? WHERE promotionId = ? AND redoubling != 1',
                [promotionToId, promotionFromId],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    );
};


module.exports = users;
