const {StandardErrors, Table} = require('../utils');

const phases = new Table('phases');
phases.selectAllByPromotionAndYear = (sql, fields, promotionId, yearId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM phases WHERE `promotionId` = ? AND `yearId` = ? ', [fields, promotionId, yearId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                });
        }
    )
};
phases.selectAllByUser = (sql, userId) => {
    return new Promise(
        (resolve, reject) => {
            const query = 'SELECT p.id, p.name, p.order, p.beginDate, p.endDate, p.active, p.config, p.finish, p.yearId FROM phases p INNER JOIN users u ON u.promotionId = p.promotionId WHERE u.id = ? AND p.yearId = (SELECT y.id FROM years y WHERE y.active = 1)';
            sql.query(query,
                [userId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                });
        }
    );
};
phases.selectByActiveYear = (sql, fields, promotionId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM `phases` WHERE `promotionId` = ? AND `yearId` = (SELECT `id` FROM `years` WHERE `years`.`active` = 1)', [fields, promotionId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                });
        }
    );
};
phases.selectActivePhaseOfUser = (sql, userId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query("SELECT config from phases WHERE phases.active = 1 AND phases.finish = 0 AND yearId = (SELECT id from years WHERE years.active = 1) AND promotionId = (SELECT promotionId FROM users WHERE id = ?)", [userId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            resolve(result[0])
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                }
            );
        }
    )
};
phases.activePhase = (sql, phaseId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query(
                "SET @promotionId = (SELECT promotionId FROM phases WHERE id = ?);" +
                "SET @yearId = (SELECT yearId FROM phases WHERE id = ?);" +
                "UPDATE phases SET active = 0 WHERE promotionId = @promotionId AND yearId = @yearId; " +
                "UPDATE phases SET `active` = 1, `finish` = 0 WHERE id = ?;", [phaseId, phaseId, phaseId],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    )
};

module.exports = phases;
