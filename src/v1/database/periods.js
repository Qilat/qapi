const {StandardErrors, Validator, Table} = require('../utils');

const periods = new Table('periods',
    {
        'id': 'id',
        'beginDate': 'begin_date',
        'endDate': 'end_date',
        'yearId': 'year_id',
        'promotionId': 'promotion_id',
        'order': 'order',
        'group': 'group'
    },
    {
        'id': 'id',
        'begin_date': 'beginDate',
        'end_date': 'endDate',
        'year_id': 'yearId',
        'promotion_id': 'promotionId',
        'order': 'order',
        'group': 'group'
    }
    );


periods.selectAllByPromotionAndYear = (sql, fields, promotionId, yearId, group) => {
    return new Promise(
        (resolve, reject) => {

            let request = 'SELECT ?? FROM periods p WHERE p.promotion_id = ? AND p.year_id = ';
            let args = [fields, promotionId];
            if (yearId === 'active') {
                request += ' (SELECT y.id FROM years y WHERE y.active = 1) ';
            } else {
                request += ' ? ';
                args.push(yearId);
            }

            if (Validator.validateNumber(group, {min: 0})) {
                request += ' AND p.`group` IN (?, 4) ';
                args.push(group);
            }

            sql.query(request, args,
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                });
        }
    );
};
periods.selectBlockedPeriodsByUserAndActiveYear = (sql, userId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT terrains.periodId AS id FROM terrains WHERE terrains.yearId = (SELECT years.id FROM years WHERE years.active = 1) AND terrains.id IN (SELECT terrain_attributions.terrainId FROM terrain_attributions WHERE terrain_attributions.userId = ? AND terrain_attributions.state = 2)',
                [userId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                })
        }
    );
};
periods.selectGroupOfTerrain = (sql, terrainId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT periods.`group` FROM periods INNER JOIN terrains t on periods.id = t.periodId WHERE t.id = ?', [terrainId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && result.length > 0) {
                            resolve(result[0].group);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                })
        }
    )
};
periods.selectGroupByAttribution = (sql, attribId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT periods.`group` FROM periods INNER JOIN terrains t on periods.id = t.periodId INNER JOIN terrain_types tt on t.terrainTypeId = tt.id INNER JOIN terrain_attributions ta on t.id = ta.terrainId WHERE ta.id = ?',
                [attribId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && result.length > 0) {
                            resolve(result[0].group);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                });
        }
    );
};

module.exports = periods;
