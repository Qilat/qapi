const {StandardErrors, Table} = require('../utils');

const rankRequests = new Table('rank_requests',
    {
        'id': 'id',
        'userId': 'user_id',
        'rankId': 'rank_id'
    },
    {
        'id': 'id',
        'user_id': 'userId',
        'rank_id': 'rankId'
    });

rankRequests.selectByUserId = (sql, userId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT rank_requests.id, rank_requests.rank_id, ranks.name FROM `rank_requests` INNER JOIN ranks ON rank_requests.rank_id = ranks.id WHERE rank_requests.user_id = ? ', [userId],
                (err, result) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            )
        }
    )
}

rankRequests.deleteAllByUserId = (sql, userId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('DELETE FROM `rank_requests` WHERE user_id = ?', [userId],
                (err) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    )
}

rankRequests.deleteByUserIdAndRankId = (sql, userId, rankId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('DELETE FROM `rank_requests` WHERE user_id = ? AND rank_id = ?', [userId, rankId],
                (err) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            )
        }
    )
}


module.exports = rankRequests;
