const {StandardErrors, Table} = require('../utils');

const exchanges = new Table('exchanges');

exchanges.STATE = {
    STANDBY: 0,
    ACCEPTED: 1,
    REFUSED: 2
}

exchanges.selectExchangers = (sql, gardeId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT u1.id AS giverId, u2.id AS receiverId FROM garde_exchanges ge INNER JOIN gardes g1 ON ge.given_garde_id = g1.id INNER JOIN users u1 ON g1.user_id = u1.id INNER JOIN gardes g2 ON ge.wanted_garde_id = g2.id INNER JOIN users u2 ON g2.user_id = u2.id WHERE ge.id = ?',
                [gardeId],
                (err, result) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && result.length === 1) {
                            resolve(result[0]);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                }
            );
        }
    )
}

exchanges.selectGardes = (sql, exchangeId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ge.id AS exchangeId, g1.id AS givenGardeId, g1.user_id AS giverId, g2.id as wantedGardeId, g2.user_id as receiverId FROM garde_exchanges ge INNER JOIN gardes g1 ON ge.given_garde_id = g1.id INNER JOIN gardes g2 ON ge.wanted_garde_id = g2.id WHERE ge.id = ?',
                [exchangeId],
                (err, result) => {
                    if (err) {
                        console.error(err);
                        reject(err);
                    } else {
                        if (result && result.length > 0) {
                            resolve(result[0]);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                })
        }
    )
}

module.exports = exchanges;
