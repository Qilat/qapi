const {StandardErrors, Table} = require('../utils');

const gardeYearConfigs = new Table('garde_year_configs',
    {
        'id': 'id',
        'yearId': 'year_id',
        'config': 'config',
        'fileId': 'file_id'
    },
    {
        'id': 'id',
        'year_id': 'yearId',
        'config': 'config',
        'file_id': 'fileId'
    });

gardeYearConfigs.selectByYearId = (sql, yearId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT gyc.year_id AS yearId, gyc.config as config, gyc.file_id as fileId FROM garde_year_configs gyc INNER JOIN years y ON gyc.year_id = y.id WHERE gyc.year_id = ?',
                [yearId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result) {
                            if (Array.isArray(result) && result.length > 0) {
                                result[0].config = JSON.parse(result[0].config);
                                resolve(result[0]);
                                return;
                            }
                        }
                        reject(StandardErrors.OBJECT_NOT_FOUND);
                    }
                }
            );
        }
    )
};

gardeYearConfigs.updateByYearId = (sql, yearId, config) => {
    return new Promise(
        (resolve, reject) => {
            const json = JSON.stringify(config);
            sql.query('INSERT INTO garde_year_configs (year_id, config) VALUES (?, ?) ON DUPLICATE KEY UPDATE config = ?', [yearId, json, json],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    )
};

gardeYearConfigs.updateFileIdByYearId = (sql, yearId, fileId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('UPDATE garde_year_configs SET file_id = ? WHERE year_id = ?', [fileId, yearId],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    )
};

module.exports = gardeYearConfigs;
