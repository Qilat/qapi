const express = require("express");
const {TokenParser, IpLoader} = require('./middlewares');

module.exports = (router) => {

    router.use(IpLoader);

    const fiches = require('./features/fiches')(express.Router());
    router.use('/fiches', fiches);

    router.use(TokenParser);


    return router;
};
