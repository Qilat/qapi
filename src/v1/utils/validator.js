/**
 * Check if value is a json string
 * @param value checked object
 * @returns {boolean|Object} return false if the object is not a json string, else return the object represent by the json string.
 */
const isJsonString = (value) => {
    try {
        return JSON.parse(value);
    } catch (e) {
        return false;
    }
};

const formatFalseDetails = (details, reason) => {
    return details ? {valid: false, reason: reason} : false;
};

/**
 * Check if the value is a boolean
 * @param value checked object
 * @param details boolean value indicating if the desired return needs to contains the reason in case of reject (default = false)
 * @returns {{valid: boolean, reason: *}|boolean}
 */
const validateBoolean = (value, details = false) => {
    if (typeof value !== 'boolean'
        && value !== '0'
        && value !== '1'
        && value !== 'true'
        && value !== 'false')
        return formatFalseDetails(details, 'TYPE_NOT_VALID');
    return true;
};

/**
 * Check if given object is a number and if it match some conditions
 * @param value checked object
 * @param min minimum value that the given object can have (can be undefined)
 * @param max maximal value that the given object can have (can be undefined)
 * @param details boolean value indicating if the desired return need to contains the reason in case of reject (default = false)
 * @returns {{valid: boolean, reason: *}|boolean}
 */
const validateNumber = (value, {min, max}, details = false) => {
    if (typeof value === 'string')
        value = value.trim();

    if (value === '')
        return formatFalseDetails(details, 'TYPE_NOT_VALID')

    value = Number(value);

    if (Number.isNaN(value) || typeof value !== 'number')
        return formatFalseDetails(details, 'TYPE_NOT_VALID');
    if (max !== undefined && value > max)
        return formatFalseDetails(details, 'NUMBER_TOO_LARGE');
    if (min !== undefined && value < min)
        return formatFalseDetails(details, 'NUMBER_TOO_SMALL');
    return true;
};

/**
 * Check if given object is a string and if it match some conditions
 * @param value checked object
 * @param maxLength maximal length the value need to have (can be undefined)
 * @param minLength minimum length the value need to have (can be undefined)
 * @param pattern regexp pattern the value need to match with (can be undefined)
 * @param values acceptable values list
 * @param details boolean value indicating if the desired return needs to contains the reason in case of reject (default = false)
 * @returns {{valid: boolean, reason: string}|boolean} return always a boolean if the value is validated, if not, the answer depends of the \'details\' value
 */
const validateString = (value, {maxLength, minLength, pattern, values}, details = false) => {
    if (typeof value !== 'string')
        return formatFalseDetails(details, 'TYPE_NOT_VALID');
    if (maxLength && value.length > maxLength)
        return formatFalseDetails(details, 'STRING_TOO_LONG');
    if (minLength && value.length < minLength)
        return formatFalseDetails(details, 'STRING_TOO_SHORT');
    if (pattern && !value.match(pattern))
        return formatFalseDetails(details, 'PATTERN_NOT_MATCH');
    if (values
        && ((!Array.isArray(values) && typeof values === 'object' && values[value] === undefined)
            || (Array.isArray(values) && values.indexOf(value) === -1)
            || (typeof values === 'string' && values !== value)))
        return formatFalseDetails(details, 'NOT_ACCEPTABLE_VALUE');
    return true;
};

/**
 * Validate object as conform to the given format.
 * @param format format the object has to be conform to
 * @param object checked object
 * @param details boolean value indicating if the desired return needs to contains the reason in case of reject (default = false)
 * @param allowEmptyObject boolean value indicating in case of all format fields are optional, if object can have no field
 * @returns {{valid: boolean, reason: *}|boolean}
 */
const validateData = (format, object, allowEmptyObject = true, details = false) => {
    if (typeof object !== "object")
        return false;

    if (!allowEmptyObject && Object.keys(object).length === 0)
        return false;

    if (format !== undefined) {

        // check if all fields of the tested object are include in the format independently of their value
        for (let field in object) {
            if (Object.prototype.hasOwnProperty.call(object, field) !== undefined && !format.hasOwnProperty(field)) {
                const response = formatFalseDetails(details, 'TOO_MUCH_FIELD');
                if (details && response !== true) {
                    response.field = field;
                    return response;
                } else if (!details && response === false) {
                    return false;
                }
            }
        }

        // check if object contains all the fields of the format and if fields'value correspond to requirements
        for (let field in format) {
            if (format.hasOwnProperty(field) && Object.prototype.hasOwnProperty.call(object, field)) {
                let value = object[field];
                let fieldFormats = format[field];

                if (!Array.isArray(fieldFormats))
                    fieldFormats = [fieldFormats];

                let response = undefined;

                for (const fieldFormat of fieldFormats) {
                    switch (fieldFormat.type) {
                        case 'number':
                            response = validateNumber(value, fieldFormat, details);
                            break;
                        case 'string':
                            response = validateString(value, fieldFormat, details);
                            break;
                        case 'date':
                            fieldFormat.pattern = /[0-9]{4}-[01][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]/
                            response = validateString(value, fieldFormat, details);
                            break;
                        case 'email':
                            fieldFormat.pattern =/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                                response = validateString(value, fieldFormat, details);
                            break;
                        case 'boolean':
                            response = validateBoolean(value);
                            break;
                        case 'array':
                            if (!Array.isArray(value))
                                return formatFalseDetails(details, 'TYPE_NOT_VALID');
                            break;
                        default:
                            if (typeof value !== fieldFormat.type)
                                return formatFalseDetails(details, 'TYPE_NOT_VALID');
                            break;
                    }

                    if (response === true) {
                        break;
                    }
                }
                if (response !== true) {
                    console.error(field);
                    if (!details) {
                        return false;
                    } else {
                        response.field = field;
                        return response;
                    }
                }
            } else if (format[field].optional && format[field].optional === true) {
                // continue;
            } else {
                return false;
            }
        }
        return true;
    } else {
        return object !== undefined;
    }
};

module.exports = {
    isJsonString,
    validateBoolean,
    validateNumber,
    validateString,
    validateData
};
