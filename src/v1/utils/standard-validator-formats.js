
const READ_ALL = (orderByValues) => {
    return {
        offset: {
            type: 'number',
            min: 0
        },
        limit: {
            type: 'number',
            min: 0
        },
        orderBy: {
            type: 'string',
            values: orderByValues,
            optional: true
        },
        asc: {
            type: 'boolean',
            optional: true
        }
    };
}


module.exports = {
    READ_ALL,
}
