/**
 *
 * @param {Array<number>} requiredAuthorizationLevels
 * @param {Array<number>} authorizationsChecked
 * @returns {boolean} true if has authorization
 */
const authorize = (requiredAuthorizationLevels, authorizationsChecked) => {
    if (!Array.isArray(authorizationsChecked) && typeof authorizationsChecked === 'number')
        authorizationsChecked = [authorizationsChecked];
    for (let level of authorizationsChecked) {
        if (requiredAuthorizationLevels.indexOf(level) !== -1) {
            return true;
        }
    }
    return false;
};

module.exports = {
    authorize
}
