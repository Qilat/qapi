const Validator = require('./validator');
const Responder = require('./responder');
const StandardErrors = require('./standard-errors');
const BooleanParser = require('./boolean-parser');

class RESTRoute {

    constructor(table, createFormat, updateFormat) {
        this.table = table;
        this.createFormat = createFormat;
        this.updateFormat = updateFormat;
        this.defaultSelectFields = [];

        this.create = this.create.bind(this);
        this.read = this.read.bind(this);
        this.readAll = this.readAll.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
    }

    create(req, res, next) {
        let object = req.body;
        delete object.id;
        this.table.insert(req.sql, object)
            .then(Responder.sendDefaultSuccess({req, res, next}))
            .catch(Responder.sendDefaultError({req, res, next}));
    }

    read(req, res, next) {
        this.table.selectById(req.sql, req.params.id, this.defaultSelectFields.slice())
            .then(Responder.sendDefaultData({req, res, next}))
            .catch(Responder.sendDefaultError({req, res, next}));
    }

    readAll(req, res, next) {
        this.table.selectAll(req.sql, this.defaultSelectFields.slice(), Number(req.query.limit), Number(req.query.offset), req.query.orderBy, BooleanParser(req.query.asc))
            .then(Responder.sendDefaultData({req, res, next}))
            .catch(Responder.sendDefaultError({req, res, next}));
    }

    update(req, res, next) {
        const id = Number(req.params.id);
        let object = req.body;
        delete object.id;

        this.table.updateById(req.sql, id, object)
            .then(Responder.sendDefaultSuccess({req, res, next}))
            .catch(Responder.sendDefaultError({req, res, next}));
    }

    delete(req, res, next) {
        const id = Number(req.params.id);
        this.table.deleteById(req.sql, id)
            .then(Responder.sendDefaultSuccess({req, res, next}))
            .catch(Responder.sendDefaultError({req, res, next}));
    }

}

module.exports = RESTRoute;
