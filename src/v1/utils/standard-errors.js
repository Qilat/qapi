const Error = require('./error');

module.exports = {
    INVALID_FORMAT: new Error(400, 'invalid-format'),

    USER_PASSWORD_MISMATCH: new Error(401, 'user-password-mismatch'),
    INVALID_TOKEN: new Error(401, 'invalid-token'),
    MISSING_TOKEN: new Error(401, 'missing-token'),

    FORBIDDEN: new Error(403, 'forbidden'),
    ALREADY_EXIST: new Error(403, 'already-exist'),

    OBJECT_NOT_FOUND: new Error(404, 'object-not-found'),

    PARAMS_TAKEN: new Error(409, 'params-taken'),

    UPLOAD_ERROR: new Error(500, 'upload-error'),
    DELETING_FILE_ERROR: new Error(500, 'deleting-file-error'),
    ERROR_OCCURS: new Error(500, 'error-occurs'),
    SQL_ERROR: new Error(500, 'sql-error'),
    MAIL_SENDING_ERROR: new Error(500, 'mail-sending-error')
}
