class Error {
    constructor(code = 500, reason = 'error-occurs') {
        this.code = code;
        this.reason = reason;
    }
}

module.exports = Error;
