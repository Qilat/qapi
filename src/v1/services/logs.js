const {LogsDB} = require('../database');

const createLogObject = (executorId, targetId, executorIp, logType, data) => {
    return {
        date: new Date().getTime(),
        executor_user_id: executorId,
        executor_user_ip: executorIp,
        target_user_id: targetId,
        type: logType,
        data: JSON.stringify(data)
    };
};

const append = (sql, executorId, targetId, executorIp, logType, data) => {
    return LogsDB.insert(sql, createLogObject(executorId, targetId, executorIp, logType, data));
};

const appendMultipleLogs = (sql, array) => {
    return LogsDB.insert(sql, array);
};


const TYPES = {
    LOGIN: 'LOGIN',
    ADD_PERSONAL_ATTRIBUTION: 'ADD_PERSONAL_ATTRIBUTION',
    REMOVE_PERSONAL_ATTRIBUTION: 'REMOVE_PERSONAL_ATTRIBUTION',
    ADD_ADMIN_ATTRIBUTION: 'ADD_ADMIN_ATTRIBUTION',
    REMOVE_ADMIN_ATTRIBUTION: 'REMOVE_ADMIN_ATTRIBUTION',
    UPDATE_ATTRIBUTION_STATE: 'UPDATE_ATTRIBUTION_STATE',
    CHANGE_PERSONAL_GROUP: 'CHANGE_PERSONAL_GROUP',
    CHANGE_ADMIN_GROUP: 'CHANGE_ADMIN_GROUP',
    CREATE_EXCHANGE: 'CREATE_EXCHANGE',
    REVOKE_EXCHANGE: 'REVOKE_EXCHANGE',
    ACCEPT_EXCHANGE: 'ACCEPT_EXCHANGE',
    RANK_REQ_ACCEPT: 'RANK_REQ_ACCEPT',
    RANK_REQ_REFUSE: 'RANK_REQ_REFUSE',
    RANK_REQ_CANCEL: 'RANK_REQ_CANCEL'
};

module.exports = {
    createLogObject,
    append,
    appendMultipleLogs,
    TYPES
};
