module.exports = {
    LogsService: require('./logs'),
    TokenService: require('./token'),
    SqlService: require('./sql'),
}
