const {SqlService} = require('../services');
const {Responder, StandardErrors} = require('../utils');

const assign = (req, res, next) => {
    SqlService.pool.getConnection(
        (error, connection) => {
            if (error) {
                console.error(error);
                Responder.sendError({req, res, next}, StandardErrors.ERROR_OCCURS);
            } else {
                req.sql = connection;
                next();
            }
        }
    )
};

module.exports = {
    assign
}
