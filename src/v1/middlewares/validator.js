const {Responder, StandardErrors, Validator} = require('../utils');

module.exports = (format, {paramsInURL, allowEmptyObject} = {
    paramsInURL: false,
    allowEmptyObject: true
}) => (req, res, next) => {
    paramsInURL = paramsInURL === undefined ? false : paramsInURL;
    allowEmptyObject = allowEmptyObject === undefined ? true : allowEmptyObject;

    let object = undefined;
    switch (req.method.toLowerCase()) {
        case 'get':
        case 'delete':
            object = req.query;
            break;
        case 'put':
        case 'post':
            object = req.body;
            break;
    }

    if (paramsInURL)
        object = req.params

    if (Validator.validateData(format, object, allowEmptyObject, false)) {
        next();
    } else {
        Responder.sendError({req, res, next}, StandardErrors.INVALID_FORMAT);
    }
}
