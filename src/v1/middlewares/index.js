module.exports = {
    Authorization: require('./authorization'),
    IpLoader: require('./ip-loader'),
    RepartitionPhaseChecker: require('./repartition-phase-checker'),
    SQL: require('./sql'),
    TokenParser: require('./token-parser'),
    Validator: require('./validator')
}
