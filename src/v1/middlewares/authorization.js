
const {Authorization, StandardErrors, Responder} = require('../utils');

module.exports = (...requiredAuthorizationLevels) => (req, res, next) => {
    if (req.payload) {
        let authorization = req.payload.authorization;
        if (requiredAuthorizationLevels.length === 0 || Authorization.authorize(authorization, requiredAuthorizationLevels)) {
            next();
        } else {
            Responder.sendError({req, res, next}, StandardErrors.FORBIDDEN);
        }
    } else {
        Responder.sendError({req, res, next}, StandardErrors.INVALID_TOKEN);
    }
};
