const {TokenService} = require('../services');
const {Responder, StandardErrors} = require('../utils');

module.exports = (req, res, next) => {
    const connection = {req, res, next};

    let token = req.headers['authorization'];

    if (token) {
        token = token.replace('Bearer ', '');
        TokenService.isTokenLegit(token, TokenService.Types.ACCESS)
            .then(
                (decoded) => {
                    req.payload = decoded.payload;
                    next();
                }
            )
            .catch(
                () => {
                    Responder.sendError(connection, StandardErrors.INVALID_TOKEN, false);
                }
            )
    } else {
        next();
    }

};
