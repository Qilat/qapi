module.exports = {
    general: {
        //prod: (process.env.NODE_ENV === "production"),
        port: process.env.PORT,
    },
    fiches : {
        folder: process.env.FICHES_FOLDER,
    },
    sql: {
        host: process.env.SQL_HOST,
        port: process.env.SQL_PORT,
        database: process.env.SQL_DATABASE,
        poolsize: process.env.SQL_POOLSIZE,
        user: process.env.SQL_USER,
        pass: process.env.SQL_PASS
    },
    file: {
        rootPath: process.env.FILE_ROOT_PATH
    },
    token: {
        publickey: process.env.TOKEN_PUBLIC_KEY,
        privatekey: process.env.TOKEN_PRIVATE_KEY
    }
};

