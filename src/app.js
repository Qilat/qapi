require('dotenv').config();
const config = require('./config');

const port = config.general.port || 8001;

const http = require('http'),
    express = require('express'),
    app = express(),
    cors = require('cors'),
    helmet = require('helmet'),
    morgan = require('morgan'),
    bodyParser = require('body-parser');

//const {SqlService} //= require('./v2/services');

app.enable('trust proxy');
app.use(helmet());
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));

const corsOptions = {
    origin: ['https://intra.qilat.fr', 'http://localhost:3000'],
    methods: ['GET', 'POST', 'OPTIONS', 'PUT', 'PATCH', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    exposedHeaders: ['Content-Disposition'],
    credentials: true,
    preflightContinue: false,
};

app.use(cors(/*corsOptions*/));

const v1 = require('./v1')(express.Router());
app.use('/v1', v1);

http.createServer(app).listen(port, () => console.log("Express server listening on port " + port + ". " + (config.general.prod === true ? 'Production' : 'Developpement') + " version"));

/*
process.on('exit', function () {
    if (SqlService) {
        SqlService.pool.close();
    }
});
 */
