#!/usr/bin/env sh
echo "Lancement de l'api de Tirajosaure"
echo "------------------------------------"
echo "1. Installation des dépendances"
npm install
echo "------------------------------------"
echo "2. Lancement via pm2"
pm2 start src/app.js --name api.qilat.fr
echo "------------------------------------"
