#!/usr/bin/env sh
echo "Arrêt de l'api de Tirajosaure"
echo "------------------------------------"
echo "1. Arrêt des processus"
pm2 stop api.qilat.fr
echo "------------------------------------"
echo "2. Suppression des processus"
pm2 del api.qilat.fr
echo "------------------------------------"
