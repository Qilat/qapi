#!/usr/bin/env sh
echo "Installation de l'api de Tirajosaure"
echo "------------------------------------"
echo "1. Installation de NPM et de NodeJS"
apt-get update
apt-get install nodejs npm
echo "------------------------------------"
echo "2. Mise à jour de NPM"
npm install npm@latest -g
echo "------------------------------------"
echo "3. Installation de pm2"
npm install pm2@latest -g
echo "------------------------------------"
echo "4. Génération de la paire de clé rsa nécessaire à la signature des tokens d'authentification"
ssh-keygen -t rsa -b 4096 -m PEM -f token.key -N ""
openssl rsa -in token.key -pubout -outform PEM -out token.key.pub
mkdir .resources
mv token.key .resources/private.key
mv token.key.pub .resources/public.key
echo "------------------------------------"
echo "5. Création des variables d'environnement"
cp .env.default .env
